#!/usr/bin/env python3
import math

rangeMin = 168630
rangeMax = 718098

count = 0

def checkIfValueIsValid(value):
	s = str(value)
	foundAdjacent = False
	foundDecrease = False
	skipTo = -1
	for x in range(0, 5):
		if (x >= skipTo):
			if (x <= 3):

				if (s[x] == s[(x + 1)]):
					if (s[x] != s[(x + 2)]):
						foundAdjacent = True
						skipTo = x + 2
					else:
						skipTo = 5
						for z in range(x + 2, 6):
							if (s[z] != s[x]):
								skipTo = z
								break
			else:
				if (s[x] == s[(x + 1)]):
					foundAdjacent = True
					skipTo = x + 2
			for y in range(x, 6):
				if (int(s[x]) > int(s[y])):
					foundDecrease = True
	if (foundAdjacent and not foundDecrease):
		return True
	else:
		return False


def assertCheckIfValueIsValid():
	assert checkIfValueIsValid(112233)
	assert checkIfValueIsValid(222345) == False
	assert checkIfValueIsValid(123444) == False
	assert checkIfValueIsValid(111122)

assertCheckIfValueIsValid()

for i in range(rangeMin, rangeMax+1):
	if (checkIfValueIsValid(i)):
		count += 1

print(count)