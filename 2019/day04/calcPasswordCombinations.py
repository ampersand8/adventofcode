#!/usr/bin/env python3
import math

rangeMin = 168630
rangeMax = 718098

count = 0

def checkIfValueIsValid(value):
	s = str(value)
	foundAdjacent = False
	foundDecrease = False
	for x in range(0, 5):
		if (s[x] == s[(x + 1)]):
			foundAdjacent = True
		for y in range(x, 6):
			if (int(s[x]) > int(s[y])):
				foundDecrease = True
	if (foundAdjacent and not foundDecrease):
		return True
	else:
		return False


for i in range(rangeMin, rangeMax+1):
	if (checkIfValueIsValid(i)):
		count += 1

print(count)