#!/usr/bin/env python3
from enum import Enum

class OpCode(Enum):
    ADD = 1
    MULTIPLY = 2
    FINISH = 99

def operate(position, intcodeArray):
    while(True):
        op = intcodeArray[position]
        if (OpCode(op) is OpCode.FINISH):
            return intcodeArray
        posN1 = intcodeArray[position + 1]
        posN2 = intcodeArray[position + 2]
        posResult = intcodeArray[position + 3]
        if (OpCode(op) is OpCode.ADD):
            intcodeArray[posResult] = intcodeArray[posN1] + intcodeArray[posN2]
            position += 4
        elif (OpCode(op) is OpCode.MULTIPLY):
            intcodeArray[posResult] = intcodeArray[posN1] * intcodeArray[posN2]
            position += 4
        else:
            raise Exception('Operation {} is not valid'.format(op))

def initialize(noun, verb, intcodeArray):
    assert 0 <= noun <= 99
    assert 0 <= verb <= 99
    intcodeArray[1] = noun
    intcodeArray[2] = verb
    return intcodeArray

def findInArray(expected, origIntcodeArray):
    result = 0
    for noun in range(0,100):
        for verb in range(0,100):
            try:
                intcodeArray = operate(0, initialize(noun, verb, origIntcodeArray.copy()))
                result = intcodeArray[0]
                if (result == expected):
                    break
            except ValueError:
                pass
        if (result == expected):
            break
    if (result != expected):
        raise Exception("Inputs for expected result {} could not be found, current intcodeArray: {}",
        expected, intcodeArray)
    print({'noun': noun, 'verb': verb})
    return (100 * noun + verb)

def assertOperate():
    assert operate(0, [1,0,0,0,99]) == [2,0,0,0,99]
    assert operate(0, [2,3,0,3,99]) == [2,3,0,6,99]
    assert operate(0, [2,4,4,5,99,0]) == [2,4,4,5,99,9801]
    assert operate(0, [1,1,1,4,99,5,6,0,99]) == [30,1,1,4,2,5,6,0,99]

assertOperate()

with open('input01', 'r') as f:
    line = f.readline()
    intcodeArray = [int(n) for n in line.split(',')]
    print(findInArray(19690720, intcodeArray))