#!/usr/bin/env python3

class Point:
	def __init__(self,x,y):
		self.x = x
		self.y = y

def separateMove(rawMove):
	return {'op': rawMove[:1], 'length': int(rawMove[1:])}

def ccw(A,B,C):
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)

# Return true if line segments AB and CD intersect
def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def findIntersection(a, b, c, d):
	if intersect(a,b,c,d):
		px = ( (a.x*b.y-a.y*b.x)*(c.x-d.x)-(a.x-b.x)*(c.x*d.y-c.y*d.x) ) / ( (a.x-b.x)*(c.y-d.y)-(a.y-b.y)*(c.x-d.x) ) 
		py = ( (a.x*b.y-a.y*b.x)*(c.y-d.y)-(a.y-b.y)*(c.x*d.y-c.y*d.x) ) / ( (a.x-b.x)*(c.y-d.y)-(a.y-b.y)*(c.x-d.x) )
		return px, py
	return 0, 0

def calcDistance(x, y):
    return abs(0 - x) + abs(0 - y)

def getNewPoint(currentPosition, rawMove):
	move = separateMove(rawMove)
	if (move['op'] == 'R'):
		currentPosition['x'] += move['length']
	elif (move['op'] == 'L'):
		currentPosition['x'] -= move['length']
	elif (move['op'] == 'U'):
		currentPosition['y'] += move['length']
	elif (move['op'] == 'D'):
		currentPosition['y'] -= move['length']
	else:
		raise Exception("Operation {} is invalid", move['op'])
	return currentPosition

def getClosestIntersection(wire1, wire2):
	previousPoint = {'x': 0, 'y': 0}
	previousPoint2 = {'x': 0, 'y': 0}
	startPoint = {'x': 0, 'y': 0}
	result = 0
	for moveLine1 in wire1:
		currentPoint = getNewPoint(previousPoint.copy(), moveLine1)
		a = Point(previousPoint['x'], previousPoint['y'])
		b = Point(currentPoint['x'], currentPoint['y'])
		previousPoint = currentPoint.copy()
		for moveLine2 in wire2:
			currentPoint2 = getNewPoint(previousPoint2.copy(), moveLine2)
			c = Point(previousPoint2['x'], previousPoint2['y'])
			d = Point(currentPoint2['x'], currentPoint2['y'])
			previousPoint2 = currentPoint2.copy()
			x, y = findIntersection(a, b, c, d)
			if x != 0 and y != 0:
				if (result == 0):
					result = calcDistance(x, y)
				result = min(result, calcDistance(x, y))
		previousPoint2 = startPoint.copy()
	return result


def assertManhattenDistance():
    assert getClosestIntersection(['R75','D30','R83','U83','L12','D49','R71','U7','L72'], ['U62','R66','U55','R34','D71','R55','D58','R83']) == 159
    assert getClosestIntersection(['R98','U47','R26','D63','R33','U87','L62','D20','R33','U53','R51'], ['U98','R91','D20','R16','D67','R40','U7','R15','U6','R7']) == 135

assertManhattenDistance()

with open('input01', 'r') as f:
	wire1 = f.readline().split(',')
	wire2 = f.readline().split(',')
	print(getClosestIntersection(wire1, wire2))