#!/usr/bin/env python3
from enum import Enum

class OpCode(Enum):
    ADD = 1
    MULTIPLY = 2
    SAVE = 3
    OUTPUT = 4
    FINISH = 99

def operate(position, intcodeArray):
    while(True):
        opAndModes = str(intcodeArray[position]).zfill(6)
        op = int(opAndModes[-2:])
        modes = ''.join(reversed(opAndModes[:-2]))
        mode = [0,0,0,0]
        for i in range(0, len(modes)):
            mode[i] = int(modes[i])
        if (OpCode(op) is OpCode.FINISH):
            print('\n')
            return intcodeArray
        if mode[0] == 0:
            posN1 = intcodeArray[position + 1]
        else:
            posN1 = position + 1
        
        if mode[1] == 0:
            posN2 = intcodeArray[position + 2]
        else:
            posN2 = position + 2
        
        if mode[2] == 0:
            posResult = intcodeArray[position + 3]
        else:
            posResult = position + 3

        if (OpCode(op) is OpCode.ADD):
            intcodeArray[posResult] = intcodeArray[posN1] + intcodeArray[posN2]
            position += 4
        elif (OpCode(op) is OpCode.MULTIPLY):
            intcodeArray[posResult] = intcodeArray[posN1] * intcodeArray[posN2]
            position += 4
        elif (OpCode(op) is OpCode.OUTPUT):
            print(intcodeArray[posN1], end = '')
            position += 2
        elif (OpCode(op) is OpCode.SAVE):
            intcodeArray[posN1] = int(input("Please provide input: "))
            position += 2
        else:
            raise Exception('Operation {} is not valid'.format(op))

def test1():
    operate(0, [1002,4,3,4,33])

test1()

with open('input01', 'r') as f:
    line = f.readline()
    intcodeArray = [int(n) for n in line.split(',')]
    operate(0, intcodeArray)