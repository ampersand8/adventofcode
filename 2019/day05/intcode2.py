#!/usr/bin/env python3
from enum import Enum


class OpCode(Enum):
	ADD = 1
	MULTIPLY = 2
	SAVE = 3
	OUTPUT = 4
	JUMP_TRUE = 5
	JUMP_FALSE = 6
	LESS_THAN = 7
	EQUALS = 8
	FINISH = 99


def operate(position, intcodeArray):
	result = []
	while(True):
		opAndModes = str(intcodeArray[position]).zfill(6)
		op = int(opAndModes[-2:])
		modes = ''.join(reversed(opAndModes[:-2]))
		mode = [0, 0, 0, 0]
		for i in range(0, len(modes)):
			mode[i] = int(modes[i])
		if (OpCode(op) is OpCode.FINISH):
			return result
		if mode[0] == 0:
			posN1 = intcodeArray[position + 1]
		else:
			posN1 = position + 1

		if mode[1] == 0:
			posN2 = intcodeArray[position + 2]
		else:
			posN2 = position + 2

		if mode[2] == 0:
			try:
				posResult = intcodeArray[position + 3]
			except IndexError:
				posResult = None
		else:
			posResult = position + 3

		if (OpCode(op) is OpCode.ADD):
			intcodeArray[posResult] = intcodeArray[posN1] + intcodeArray[posN2]
			position += 4
		elif (OpCode(op) is OpCode.MULTIPLY):
			intcodeArray[posResult] = intcodeArray[posN1] * intcodeArray[posN2]
			position += 4
		elif (OpCode(op) is OpCode.OUTPUT):
			result.append(intcodeArray[posN1])
			position += 2
		elif (OpCode(op) is OpCode.SAVE):
			intcodeArray[posN1] = int(input("Please provide input: "))
			position += 2
		elif (OpCode(op) is OpCode.JUMP_TRUE):
			if intcodeArray[posN1] != 0:
				position = intcodeArray[posN2]
			else:
				position += 3
		elif (OpCode(op) is OpCode.JUMP_FALSE):
			if intcodeArray[posN1] == 0:
				position = intcodeArray[posN2]
			else:
				position += 3
		elif (OpCode(op) is OpCode.LESS_THAN):
			if (intcodeArray[posN1] < intcodeArray[posN2]):
				intcodeArray[posResult] = 1
			else:
				intcodeArray[posResult] = 0
			position += 4
		elif (OpCode(op) is OpCode.EQUALS):
			if (intcodeArray[posN1] == intcodeArray[posN2]):
				intcodeArray[posResult] = 1
			else:
				intcodeArray[posResult] = 0
			position += 4
		else:
			raise Exception('Operation {} is not valid'.format(op))
	return result


def test1():
	#operate(0, [1002,4,3,4,33])
	#print(operate(0, [3,9,8,9,10,9,4,9,99,-1,8])) # position mode equal to 8
	#print(operate(0, [3,9,7,9,10,9,4,9,99,-1,8])) # position mode less than 8
	#print(operate(0, [3,3,1108,-1,8,3,4,3,99]))   # immediate mode equal to 8
	#print(operate(0, [3,3,1107,-1,8,3,4,3,99]))   # immediate mode less than 8
	#print(operate(0, [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9])) # position mode jump
	#print(operate(0, [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]))      # immediate mode jump
	print(operate(0, [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]))
	#print(''.join(str(n) for n in opresult))

with open('input01', 'r') as f:
	line = f.readline()
	intcodeArray = [int(n) for n in line.split(',')]
	print(operate(0, intcodeArray))
