#!/usr/bin/env python3
import math

def calcFuelPerModule(mass):
	return math.floor((mass / 3)) - 2

def assertCorrectness():
	assert calcFuelPerModule(12) == 2
	assert calcFuelPerModule(14) == 2
	assert calcFuelPerModule(1969) == 654
	assert calcFuelPerModule(100756) == 33583

assertCorrectness()

with open('input01', 'r') as masses:
	mass = masses.readline()
	totalFuel = 0
	while mass:
		totalFuel += calcFuelPerModule(int(mass))
		mass = masses.readline()
	print("Total required fuel: {}".format(totalFuel))