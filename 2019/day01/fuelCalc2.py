#!/usr/bin/env python3
import math

def calcFuelPerModule(mass):
	return math.floor((mass / 3)) - 2

def assertFuelPerModule():
	assert calcFuelPerModule(2) <= 0
	assert calcFuelPerModule(12) == 2
	assert calcFuelPerModule(14) == 2
	assert calcFuelPerModule(1969) == 654
	assert calcFuelPerModule(100756) == 33583

def calcFuelForFuel(mass):
	totalFuel = 0
	fuel = calcFuelPerModule(mass)
	while (fuel > 0):
		totalFuel += fuel
		fuel = calcFuelPerModule(fuel)
	return totalFuel

def assertFuelForFuel():
	assert calcFuelForFuel(14) == 2
	assert calcFuelForFuel(1969) == 966
	assert calcFuelForFuel(100756) == 50346

assertFuelPerModule()
assertFuelForFuel()

with open('input01', 'r') as masses:
	mass = masses.readline()
	totalFuel = 0
	while mass:
		totalFuel += calcFuelForFuel(int(mass))
		mass = masses.readline()
	print("Total required fuel: {}".format(totalFuel))
